<?php get_header(); ?>
<div class="container-fluid user">
  <div class="row">
    <div class="col">
      <h2> <?php echo $wp_query->found_posts; ?> Search Result(s) Found For: "<?php the_search_query(); ?>" </h2>
      <div class="card-columns">
        <?php while(have_posts()) : the_post(); ?>
        <div class="card">
          <?php if(has_post_thumbnail()) : ?>
          <div class="card-thumb">
            <?php the_post_thumbnail(); ?>
          </div>
        <?php endif ?>
          <div class="card-body">
            <h4 class="card-title"><?php the_title(); ?>
              <!-- Add 'NEW' Badge if Post is newer then 10 days -->
              <?php if(( time() - 864000 ) < get_the_date('U')) : ?><span class="badge badge-pill badge-dark">New</span><?php endif ?>
            </h4>
            <p class="card-text"><small><?php the_time('F dS, Y G:i'); ?> by <?php the_author(); ?></small></p>
            <p class="card-text"><?php the_excerpt(); ?></p>
            <a class="btn btn-primary" href="<?php the_permalink(); ?>" role="button">Read More</a>
          </div>
        </div>
        <?php endwhile; ?>
      </div>
    </div>
    <div class="col-sm-auto sidebar">
    <?php if(is_active_sidebar('sidebar')): ?>
      <?php dynamic_sidebar('sidebar'); ?>
    <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
