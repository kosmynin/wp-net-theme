<?php
  // Register Nav Walker class
  require_once 'wp_bootstrap_navwalker.php';

  // Theme support
  function wpb_theme_setup(){
    // Support Thumbnails
    add_theme_support('post-thumbnails');
    // Nav menus
    register_nav_menus(array(
      'primary' => __('Primary Menu')
    ));
  }

  add_action('after_setup_theme','wpb_theme_setup');

  function wpb_init_widgets($id){
    register_sidebar(array(
      'name'          => 'Sidebar',
      'id'            => 'sidebar',
      'before_widget' => '<div class="container">',
      'after_widget'  => '</div>',
      'before_title'  => '<h4>',
      'after_title'   => '</h4>'
    ));
  }

  add_action('widgets_init','wpb_init_widgets');

//Exclude pages from WordPress Search
if (!is_admin()) {
  function wpb_search_filter($query) {
  if ($query->is_search) {
  $query->set('post_type', 'post');
  }
  return $query;
  }
  add_filter('pre_get_posts','wpb_search_filter');
  }
