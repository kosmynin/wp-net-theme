<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <!-- Metadata -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="content-type" content="text/html; charset=UTF-9">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0, shrink-to-fit=no">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <!-- Title -->
    <title>
      <?php bloginfo('name'); ?> |
      <?php is_front_page() ? bloginfo('description') : wp_title(); ?>
    </title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <!-- Devicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/devicon/2.2/devicon.css">
    <!-- Wordpress Header -->
    <?php wp_head(); ?>
  </head>

  <body>
    <!-- Particle Animation -->
    <?php if(is_front_page()) : ?>
    <div id="particles-js">
      <div class="content">
        <h0>
          <!-- Site Title -->
          <span class="site-title"><?php bloginfo('name'); ?></span>
          <!-- Site Description -->
          <span class="site-description"><?php bloginfo('description'); ?></span>
        </h0>
      </div>
      <a class="down" href="#home" data-scroll=""><i class="icon fa fa-chevron-down" aria-hidden="true"></i></a>
    </div>
    <?php endif ?>
    <!-- Navigation Bar -->
    <nav class="navbar navbar-expand-lg sticky-top navbar-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <?php
    	wp_nav_menu( array(
    		'theme_location'    => 'primary',
    		'depth'             => 2,
    		'container'         => 'div',
    		'container_class'   => 'collapse navbar-collapse',
    		'container_id'      => 'navbar',
    		'menu_class'        => 'nav navbar-nav',
    		'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
    		'walker'            => new WP_Bootstrap_Navwalker(),
    	) );
      ?>
      <form role="search" method="get" id="searchform" class="form-inline" action="<?php echo get_home_url();?>">
        <div class="input-group">
          <input class="form-control" type="text" value="" name="s" id="s" placeholder="Search">
          <div class="input-group-append">
            <button class="btn btn" type="submit" id="searchsubmit">
            <i class="icon fa fa-search"></i></button>
          </div>
        </div>
			</form>
    </nav>
