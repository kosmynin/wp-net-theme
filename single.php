<?php get_header(); ?>
<div class="container-fluid">
  <div class="row">
    <?php while(have_posts()) : the_post(); ?>
      <div class="col-lg-8 single-post">
        <?php if(has_post_thumbnail()) : ?>
          <div class="text-center single-thumb">
            <?php the_post_thumbnail(); ?>
          </div>
        <?php endif ?>
        <h1><?php the_title(); ?></h1>
        <p><?php the_time('F dS, Y G:i a'); ?> by <?php the_author(); ?></p>
        <hr>
        <p><?php the_content(); ?></p>

        <?php comments_template(); ?>
      </div>
    <?php endwhile; ?>
    <div class="col-lg-2 sidebar">
      <?php if(is_active_sidebar('sidebar')): ?>
        <?php dynamic_sidebar('sidebar'); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>

