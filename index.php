<?php get_header(); ?>

    <section id="home">
      <div class="container user">
        <!-- Page Loop -->
        <?php
        $i = -1;
        foreach(get_pages() as $page) {
          if(!(($page->post_name)=='privacy-policy')) :
            $i = $i + 1;
            if($i%2==0) : ?>
            <div class="row" style="padding-top:2rem;">
            <?php endif; ?>
              <div class="col-lg text-center">
                <section id="<?php echo($page->post_name); ?>">
                  <h2><?php echo($page->post_title); ?></h2>
                  <?php echo($page->post_content); ?>
                </section>
              </div>
        <?php if($i%2==1) : ?>
          </div>
          <hr />
        <?php endif;  endif; } ?>
      </div>

      <section id="posts">
        <div class="container-fluid user">
          <!-- Post Loop -->
          <?php if(have_posts()) : ?>
          <h2 style="padding:20px"> Posts </h2>
          <div class="card-columns">
            <?php while(have_posts()) : the_post(); ?>
            <div class="card">
              <?php if(has_post_thumbnail()) : ?>
              <div class="card-thumb">
                <?php the_post_thumbnail(); ?>
              </div>
            <?php endif ?>
              <div class="card-body">
                <h4 class="card-title"><?php the_title(); ?>
                  <!-- Add 'NEW' Badge if Post is newer then 30 days -->
                  <?php if(( time() - 2592000 ) < get_the_date('U')) : ?><span class="badge badge-pill badge-dark">New</span><?php endif ?>
                </h4>
                <p class="card-text"><small><?php the_time('F dS, Y G:i'); ?> by <?php the_author(); ?></small></p>
                <p class="card-text"><?php the_excerpt(); ?></p>
                <a class="btn btn-primary" href="<?php the_permalink(); ?>" role="button">Read More</a>
              </div>
            </div>
          <?php endwhile; ?>
            <?php else : ?>
              <!-- Show Alert if no Posts are available -->
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Looks like I didn't post anything!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </section>
    </section>

<?php get_footer(); ?>
