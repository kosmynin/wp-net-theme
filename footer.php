    <!-- Footer -->
    <footer class="footer">
      <div class="container">
        <p>&copy; <?php echo date('Y'); ?> - <?php bloginfo('name'); ?></p>
      </div>
    </footer>

    <!-- Wordpress footer -->
    <?php wp_footer(); ?>
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <!-- popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <!-- Bootstrap core js -->
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js"></script>
    <!-- Custom js for particle effect -->
    <script src="<?php bloginfo('template_url'); ?>/js/app.js"></script>
    <!-- Particle js -->
    <script src="<?php bloginfo('template_url'); ?>/js/particles.js"></script>
    <!-- Sweet scroll js-->
    <script src="<?php bloginfo('template_url'); ?>/js/sweet-scroll.js"></script>
  </body>
</html>
